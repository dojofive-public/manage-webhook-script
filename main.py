import requests
import os
import yaml
import argparse
import json
from dataclasses import dataclass

#DO NOT CHECK THIS IN
#Particle API Token that allows access to Particle's API
API_TOKEN = os.environ['PARTICLE_API_TOKEN']

@dataclass
class webhookInfo:
    # dataclass to hold webtoken info
    wh_id: str
    name: str
    url: str
    requestType: str

#Setup 'pretend' and 'update' options
#If neither pretend or update options are passed into the argument, the
#  script will return a list of webhooks that are only in the config.yml
#  and webhooks that are only in the Particle Backend.
#If both options are passed, 'pretend' will take priority
parser = argparse.ArgumentParser(description='Testing HTTP requests with Particle.')
parser.add_argument('--pretend', '-p', action='store_true',
                   help='Don\'t actually make any changes to the Particle backend.')
parser.add_argument('--update', '-u', action='store_true',
                   help='Update webhooks on Particle backend.')
args = parser.parse_args()

def parse_json_to_webhookInfo(object):
    """Parses webhook recieved over HTTP request to webhookInfo Object."""
    webhook_info = webhookInfo(
            object['id'],
            object['event'],
            object['url'],
            object['requestType']
        )
    return webhook_info

def get_product_webhooks():
    """Gets webhooks from Particle backend.
    - Returns array of webhookInfo objects"""

    #Create HTTP Request to GET webhooks
    params = (
        ('access_token', API_TOKEN),
    )
    response = requests.get('https://api.particle.io/v1/integrations', params=params)
    json_object = json.loads(response.content)
    current_webhooks = []
    for obj in json_object:
        current_webhooks.append(parse_json_to_webhookInfo(obj))
    return current_webhooks

def create_webhook(webhook_info):
    """Creates webhook on Particle backend."""

    #Create HTTP Request to create webhook
    params = (
        ('access_token', API_TOKEN),
    )
    data = {
        'integration_type': 'Webhook',
        'event': webhook_info.name,
        'url': webhook_info.url,
        'requestType': webhook_info.requestType
    }
    response = requests.post('https://api.particle.io/v1/integrations', params=params, data=data)
    if(response.status_code == 201):
        return True
    else:
        return False

def delete_webhook(webhook):
    """Deletes webhook off Particle backend"""

    #Create HTTP Request to delete webhook
    request_url = 'https://api.particle.io/v1/integrations/' + webhook.wh_id
    params = (
        ('access_token', API_TOKEN),
    )
    delete_response = requests.delete(
                request_url, params=params)
    if delete_response.status_code == 204:
        return True
    else:
        return False

def parse_yaml_webhook(config_file='config.yml'):
    """Parses webhookInfo objects from config.yml"""

    webhooks = []
    with open(config_file) as file:
        configuration = yaml.full_load(file)
    for webhook in configuration["webhooks"]:
        webhook_info = webhookInfo(
            None,
            webhook["name"],
            webhook["url"],
            webhook["requestType"])
        webhooks.append(webhook_info)
    return webhooks

def compare_webhook(webhook_info, current_webhooks):
    """Checks if a singular webhook is the same as any in an array of webhook objects"""

    for obj in current_webhooks:
        if webhook_info.name == \
          obj.name and webhook_info.url == \
          obj.url and webhook_info.requestType == obj.requestType:
            return True
    return False

def manage_webhooks(yaml_webhooks, current_webhooks):
    """Manages webhooks on both Particle backend and config.yml"""

    # Check if yaml_webhooks are on Particle Backend
    print("\nChecking webhooks in config.yml")
    for webhook in yaml_webhooks:
        if compare_webhook(webhook, current_webhooks)==False:
            print(webhook.name + " is not on Particle backened.")
            if args.pretend:
                print("If not pretending, would add " + webhook.name + " to Particle backend.\n")
            elif args.update:
                print("Adding " + webhook.name + " to Particle backend...")
                if create_webhook(webhook):
                    print("Addition Successful\n")
                else:
                    print("Addition Failed\n")
    # Check if particle webhooks are in yaml config
    print("\nChecking webhooks on Particle Backend")
    for webhook in current_webhooks:
        if compare_webhook(webhook, yaml_webhooks)==False:
            print(webhook.name + " is not in config.yml.")
            if args.pretend:
                print("If not pretending, would delete " + webhook.name + " from Particle backend.\n")
            elif args.update:
                print("Deleting " + webhook.name + " from Particle backend...")
                if delete_webhook(webhook):
                    print("Deletion Successful\n")
                else:
                    print("Deletion Failed\n")

def main():
    if args.pretend:
        print("Pretend: ON\nNo changes will actually be made.")
    elif args.update:
        print("Update: ON\nChanges will be made to the backend.")

    #Grab and manage webhooks
    yaml_webhooks = parse_yaml_webhook()
    current_webhooks = get_product_webhooks()
    manage_webhooks(yaml_webhooks, current_webhooks)

if __name__ == "__main__":
    main()
