particle-http-checker
=====================

particle-http-checker compares configured Particle webhooks with a configuration file.

It can change the Particle backend to match the config file, or just report.

Installation
------------
Download this repository and install the dependencies in requirements.txt.  You can do this with `pip install -r requirements.txt` but you may want to be in a virtualenv first (`python3 -m venv venv; source venv/bin/activate`).

API-TOKEN
---------
This program requires the use of an API token from Particle in your environment variables.
Creating an API Token:
Through Particle CLI: https://docs.particle.io/reference/developer-tools/cli/#particle-token-create
Through curl command: https://docs.particle.io/reference/device-cloud/api/#generate-an-access-token

MacOS:
In your `.zshrc` file(which can be found in your home directory ~/.zshrc), add
    `extern PARTICLE_API_TOKEN=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx` 
to add your Particle API Token to the environment variables. 
Note: If .zshrc doesn't exist, you will need to create it.

Linux/WSL(Windows Subsytem for Linux):
About WSL: https://docs.microsoft.com/en-us/windows/wsl/about
In your `.bashrc` file(which can be found in your home directory ~/.zshrc),  add
    `export PARTICLE_API_TOKEN=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx` 
to add your Particle API Token to the environment variables. 
Note: If the script isn't finding 'PARTICLE_API_TOKEN', you may need to run `source ~/.bashrc` for the script to recognize it.

Usage
----- 
config.yml is where the webhooks will be stored for the Particle backend. Each webhook will consist of three fields: name, url and response type.
Example with two webhooks:
```
webhooks:
  - name: test_webhook
    url: https://samplesite.com
    requestType: POST
  - name: second_webhook
    url: https://secondsamplesite.com
    requestType: POST
```
Ensure that the config.yml is in the same directory as `main.py`.

To run the program, just run `/path/to/repo/main.py`
This will provide a list of webhooks that are only present in the config.yml, and a list of webhooks only present in the Particle backend.
Example:
```
Checking webhooks in config.yml
second_webhook is not on Particle backened.
Checking webhooks on Particle Backend
temp is not in config.yml.
```

There is a `--pretend` or `-p` option that will allow the user to see what changes would happen if the program were to be run with the
update function without the script actually making these changes.
`/path/to/repo/main.py -p` or `/path/to/repo/main.py --pretend`
```
Pretend: ON
No changes will actually be made.

Checking webhooks in config.yml
second_webhook is not on Particle backened.
If not pretending, would add second_webhook to Particle backend.


Checking webhooks on Particle Backend
temp is not in config.yml.
If not pretending, would delete temp from Particle backend.
```

Finally, there is an `--update` or `-u` option that will allow the user to make changes to the webhooks on the Particle backend.
The upgrade option will delete and add webhooks so that the Particle backends will match the webhooks in the config.yml.
`/path/to/repo/main.py -u` or `/path/to/repo/main.py --upgrade`
```
Update: ON
Changes will be made to the backend.

Checking webhooks in config.yml
second_webhook is not on Particle backened.
Adding second_webhook to Particle backend...
Addition Successful


Checking webhooks on Particle Backend
temp is not in config.yml.
Deleting temp from Particle backend...
Deletion Successful
```